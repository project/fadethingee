<?php

//
//	from ZEN // Override or insert PHPTemplate variables into the page templates.
//
//	 This function creates the body classes that are relative to each page
//
//	@param $vars
//	  A sequential array of variables to pass to the theme template.
//	@param $hook
//	  The name of the theme function being called ("page" in this case.)
//

function fadethingee_preprocess_page(&$vars, $hook) {
  global $theme;

  // Don't display empty help from node_help().
  if ($vars['help'] == "<div class=\"help\"><p></p>\n</div>") {
    $vars['help'] = '';
  }

  // Classes for body element. Allows advanced theming based on context
  // (home page, node of certain type, etc.)
  $body_classes = array($vars['body_classes']);
  if (!$vars['is_front']) {
    // Add unique classes for each page and website section
    $path = drupal_get_path_alias($_GET['q']);
    list($section, ) = explode('/', $path, 2);
    $body_classes[] = fadethingee_id_safe('page-'. $path);
    $body_classes[] = fadethingee_id_safe('section-'. $section);
    if (arg(0) == 'node') {
      if (arg(1) == 'add') {
        if ($section == 'node') {
          array_pop($body_classes); // Remove 'section-node'
        }
        $body_classes[] = 'section-node-add'; // Add 'section-node-add'
      }
      elseif (is_numeric(arg(1)) && (arg(2) == 'edit' || arg(2) == 'delete')) {
        if ($section == 'node') {
          array_pop($body_classes); // Remove 'section-node'
        }
        $body_classes[] = 'section-node-'. arg(2); // Add 'section-node-edit' or 'section-node-delete'
      }
    }
  }
  $vars['body_classes'] = implode(' ', $body_classes); // Concatenate with spaces

     // define how many 960gs grid units per column - using the 16 grid unit system
  $layout = $vars['layout'];
   switch ($layout) {
     case 'left':
         $vars['gridunits'] = array(4,12,0);
           break;
     case 'right':
        $vars['gridunits'] = array(0,12,4);
           break;
     case 'both':
         $vars['gridunits'] = array(4,8,4);
           break;
     case 'none':
         $vars['gridunits'] = array(0,16,0);
          break;
  }
}



//
//	from ZEN // Override or insert PHPTemplate variables into the node templates.
//
//	 This function creates the NODES classes, like 'node-unpublished' for nodes
//	 that are not published, or 'node-mine' for node posted by the connected user...
//
//	@param $vars
//	  A sequential array of variables to pass to the theme template.
//	@param $hook
//	  The name of the theme function being called ("node" in this case.)
//

function fadethingee_preprocess_node(&$vars, $hook) {
  global $user;

  // Special classes for nodes
  $node_classes = array();
  if ($vars['sticky']) {
    $node_classes[] = 'sticky';
  }
  if (!$vars['node']->status) {
    $node_classes[] = 'node-unpublished';
    $vars['unpublished'] = TRUE;
  }
  else {
    $vars['unpublished'] = FALSE;
  }
  if ($vars['node']->uid && $vars['node']->uid == $user->uid) {
    // Node is authored by current user
    $node_classes[] = 'node-mine';
  }
  if ($vars['teaser']) {
    // Node is displayed as teaser
    $node_classes[] = 'node-teaser';
  }
  // Class for node type: "node-type-page", "node-type-story", "node-type-my-custom-type", etc.
  $node_classes[] = 'node-type-'. $vars['node']->type;
  $vars['node_classes'] = implode(' ', $node_classes); // Concatenate with spaces
}


//
// from ZEN // Override or insert PHPTemplate variables into the block templates.
//
//	This function create the EDIT LINKS for blocks and menus blocks.
//	When overing a block (except in IE6), some links appear to edit
//	or configure the block. You can then edit the block, and once you are
//	done, brought back to the first page.
//
// @param $vars
//   A sequential array of variables to pass to the theme template.
// @param $hook
//   The name of the theme function being called ("block" in this case.)
//

function fadethingee_preprocess_block(&$vars, $hook) {
  $block = $vars['block'];

  if (user_access('administer blocks')) {
    // Display 'edit block' for custom blocks
    if ($block->module == 'block') {
      $edit_links[] = l( t('edit block'), 'admin/build/block/configure/'. $block->module .'/'. $block->delta, array('title' => t('edit the content of this block'), 'class' => 'block-edit'), drupal_get_destination(), NULL, FALSE, TRUE);
    }
    // Display 'configure' for other blocks
    else {
      $edit_links[] = l(t('configure'), 'admin/build/block/configure/'. $block->module .'/'. $block->delta, array('title' => t('configure this block'), 'class' => 'block-config'), drupal_get_destination(), NULL, FALSE, TRUE);
    }

    // Display 'edit menu' for menu blocks
    if (($block->module == 'menu' || ($block->module == 'user' && $block->delta == 1)) && user_access('administer menu')) {
      $edit_links[] = l(t('edit menu'), 'admin/build/menu', array('title' => t('edit the menu that defines this block'), 'class' => 'block-edit-menu'), drupal_get_destination(), NULL, FALSE, TRUE);
    }
    $vars['edit_links_array'] = $edit_links;
    $vars['edit_links'] = '<div class="edit">'. implode(' ', $edit_links) .'</div>';
  }
}


//
//  Create some custom classes for comments
//

function comment_classes($comment) {
  $node = node_load($comment->nid);
  global $user;

  $output .= ($comment->new) ? ' comment-new' : '';
  $output .=  ' '. $status .' ';
  if ($node->name == $comment->name) {
    $output .= 'node-author';
  }
  if ($user->name == $comment->name) {
    $output .=  ' mine';
  }
  return $output;
}

/**
 * Converts a string to an id.
 *
 * @param $string
 *   The string
 * @return
 *   The converted string
 *
 * @see http://drupal.org/project/zen
 */
function fadethingee_id_safe($string) {
  $string = strtolower(preg_replace('/[^a-zA-Z0-9_-]+/', '-', $string));
  if (!ctype_lower($string{0})) {
    $string = 'id'. $string;
  }
  return $string;
}

/**
* Override the theme_links function
*
* We use this to insert <span></span> tags around anchor text in the
* primary and secondary links. We need these to support Internet Explorer
* when building sliding door tabs with hover effects.
*/
function fadethingee_links($links, $attributes =  array('class' => 'links')) {
  $output = '';
  if (count($links) > 0) {
    $output = '<ul'. drupal_attributes($attributes) .'>';

    $num_links = count($links);
    $i = 1;

    foreach ($links as $key => $link) {
      $class = $key;

      // Add first, last and active classes to the list of links to help out themers.
      if ($i == 1) {
        $class .= ' first';
      }
      if ($i == $num_links) {
        $class .= ' last';
      }
      if (isset($link['href']) && ($link['href'] == $_GET['q'] || ($link['href'] == '<front>' && drupal_is_front_page()))) {
        $class .= ' active';
      }

      $output .= '<li'. drupal_attributes(array('class' => $class)) .'>';

      // wrap <span>'s around the anchor text
      if (isset($link['href'])) {
        $link['title'] = '<span>' . check_plain($link['title']) . '</span>';
        $link['html'] = TRUE;
        // Pass in $link as $options, they share the same keys.
        $output .= l($link['title'], $link['href'], $link);
      }
      else if (!empty($link['title'])) {
        // Some links are actually not links, but we wrap these in <span> for adding title and class attributes
        if (empty($link['html'])) {
          $link['title'] = check_plain($link['title']);
        }
        $span_attributes = '';
        if (isset($link['attributes'])) {
          $span_attributes = drupal_attributes($link['attributes']);
        }
        $output .= '<span'. $span_attributes .'>'. $link['title'] .'</span>';
      }

      $i++;
      $output .= "</li>\n";
    }

    $output .= '</ul>';
  }
  return $output;
}
/**
*
* Add a class to each li element
*
*/
function fadethingee_menu_item($link, $has_children, $menu = '', $in_active_trail = FALSE, $extra_class = NULL) {
  $class = ($menu ? 'expanded' : ($has_children ? 'collapsed' : 'leaf'));
  if (!empty($extra_class)) {
    $class .= ' '. $extra_class;
  }
  if ($in_active_trail) {
    $class .= ' active-trail';
  }
  $id = preg_replace("/[^a-zA-Z0-9]/", "", strip_tags($link));
  return '<li id="'.$id.'" class="'. $class .'">'. $link . $menu ."</li>\n";
}

//
// 	Customize the PRIMARY and SECONDARY LINKS, to allow the admin tabs to work on all browsers
// 	Generate the HTML representing a given menu item ID.
// 	An implementation of theme_menu_item_link()
//
// 	@param $link
// 	  array The menu item to render.
// 	@return
// 	  string The rendered menu item.
//
function fadethingee_menu_item_link($link) {
  if (empty($link['localized_options'])) {
    $link['localized_options'] = array();
  }
		
  // If an item is a LOCAL TASK, render it as a tab.
  if ($link['type'] & MENU_IS_LOCAL_TASK) {
    $link['title'] = '<span class="tab">'. check_plain($link['title']) .'</span>';
    $link['localized_options']['html'] = TRUE;
  }

  return l($link['title'], $link['href'], $link['localized_options']);
}

/**
 * Duplicate of theme_menu_local_tasks() but adds clear-block to tabs.
 */
function fadethingee_menu_local_tasks() {
  $output = '';

  if ($primary = menu_primary_local_tasks()) {
    $output .= '<ul class="tabs primary clear-block">'. $primary .'</ul>';
  }
  if ($secondary = menu_secondary_local_tasks()) {
    $output .= '<ul class="tabs secondary clear-block">'. $secondary .'</ul>';
  }

  return $output;
}