<?php
?>
<?php
/* fadethingee by webthingee licensed as GPL
*
*/
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
  "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<?php echo $language->language ?>" lang="<?php echo $language->language ?>" dir="<?php echo $language->dir ?>">

<head>
  <title><?php echo $head_title; ?></title>
  <?php echo $head; ?>
  <?php echo $styles; ?>
  <?php echo $scripts; ?>
</head>
<body class="thingee <?php print $body_classes; ?>">
  <div id="wrapper">
    <div id="wrapper-header">
      <div id="header" class="container_16">

        <div id="secondary" class="grid_16 textright clear-block">
        <?php if (!empty($secondary_links)): ?>
        <?php echo theme('links', $secondary_links, array('class' => 'links secondary-links')); ?>
        <?php endif; ?>
        </div>
        <!-- /secondary links -->

        <div class="clear">&nbsp;</div>

        <div id="header-main" class="grid_16">

  	  	  <div id="logo">
          <?php if (!empty($logo)): ?>
            <a href="<?php echo $front_page; ?>" title="<?php echo $site_name; ?>" rel="home">
            <img src="<?php echo $logo; ?>" alt="<?php echo $site_name; ?>" />
            </a>
          <?php endif; ?>
          </div><!-- /logo -->

          <div id="site-name">
            <?php if (!empty($site_name)): ?>
            <h1 class="site-name">
            <a href="<?php echo $front_page ?>" title="<?php echo t('Home'); ?>" rel="home"><span><?php echo $site_name; ?></span></a>
            </h1>
            <?php endif; ?>
          </div><!-- /site-name -->

          <?php if (!empty($site_slogan)): ?>
          <div id="site-slogan"><?php echo $site_slogan; ?>
          </div>
          <?php endif; ?>

	      </div>
        <!-- /header-main -->


        <div class="clear">&nbsp;</div>
      </div><!-- /header /.container 16 -->

    </div><!-- /wrapper-head -->

    <div id="wrapper-primarylinks">
      <?php if (!empty($primary_links)): ?>
      <div id="primary" class="container_16 clear-block">
      <?php echo theme('links', $primary_links, array('class' => 'links primary-links')); ?>
      </div>
      <?php endif; ?>

      <div class="clear">&nbsp;</div>
    </div>
    <!-- /wrapper-primarylinks -->

    <div id="wrapper-middle">
      <div id="middle" class="container_16 with-right with-left">
        <div class="grid_<?php print $gridunits[1] ?> prefix_<?php print $gridunits[0] ?> suffix_<?php print $gridunits[2] ?>">
          <div class="breadcrumb">
          <?php echo $breadcrumb; ?>
          </div>
        </div>

        <?php if ($left): ?>
        <div id="left-sidebar" class="grid_<?php print $gridunits[0] ?> sidebar" >
        <?php echo $left; ?>
        </div>
        <?php endif; ?>

        <div id="main" class="grid_<?php print $gridunits[1] ?>">
          <?php if ($content_top): ?>
          <?php echo $content_top; ?>
          <?php endif; ?>

          <div id="content">
          	<?php if ($title or $tabs or $help or $messages or $mission): ?>
          	<div id="content-header">
          	<?php if ($title): ?>
          	<h1 class="title"><?php echo $title; ?></h1>
          	<?php endif; ?>
  		  	  <?php if ($mission): ?>
  					<div id="mission"><?php echo $mission; ?></div>
  					<?php endif; ?>
          	<?php echo $messages; ?>
          	<?php if ($tabs): ?>
          	<div class="tabs"><?php echo $tabs; ?></div>
            <div class="clear">&nbsp;</div>
          	<?php endif; ?>
          	<?php echo $help; ?>
          	</div> <!-- /#content-header -->
          	<?php endif; ?>

            <!-- CONTENT AREA -->
            <!--<div id="content-inner">-->
            <?php echo $content; ?>
            <!--</div>-->

            <?php echo $feed_icons; ?>

  	  		  <?php if ($content_bottom): ?>
            <div id="content-bottom">
            <?php echo $content_bottom; ?>
            </div>
            <?php endif; ?>


          </div>
          <!-- /content -->
        </div>
        <!-- /main -->

        <?php if ($right): ?>
        <div id="right-sidebar" class="grid_<?php print $gridunits[2] ?> sidebar" >
        <?php echo $right; ?>
        </div>
        <?php endif; ?>

        <div class="clear">&nbsp;</div>

      </div><!-- /middle /.container 16 -->

      <div class="clear">&nbsp;</div>
    </div>
    <!-- /wrapper-middle -->

  <div id="wrapper-above-footer">
    <div id="wrapper-footer">
      <div id="footer" class="container_16">

        <?php if ($footer_pre): ?>
        <div class=id="pre-footer" class="grid_16">
        <?php echo $footer_pre; ?>
        </div>
        <?php endif; ?>

        <div class="clear">&nbsp;</div>
        <div id="mid-footer" class="grid_16">

         <?php if ($footer_left): ?>
         <div id="footer-left" class="grid_8 alpha">
         <?php echo $footer_left; ?>
         </div>
         <?php endif; ?>

         <?php if ($footer_right): ?>
         <div id="footer-right" class="grid_8 omega">
         <?php echo $footer_right; ?>
         </div>
         <?php endif; ?>


        </div>
        <div class="clear">&nbsp;</div>

        <?php if ($footer_links): ?>
        <div id="links-footer" class="grid_16">
        <?php echo $footer_links; ?>
        </div>
        <?php endif; ?>

        <div class="clear">&nbsp;</div>

        <div id="copyright-footer" class="grid_16 textright">
          <span>Copyright &copy; 2009</span> |
          <a href="http://www.webthingee.com" class="quiet">webthingee.com</a> |
          <a href="http://www.blogthingee.com" class="quiet">blogthingee.com</a>
        </div>

        <div class="clear">&nbsp;</div>

      </div><!-- /footer /.container 16 -->
      <div class="clear">&nbsp;</div>

    </div>
    <!-- /wrapper-footer -->
  </div><!-- /wrapper-above-footer -->

  <?php print $closure; ?>
</div>
<!-- /wrapper -->
</body>
</html>