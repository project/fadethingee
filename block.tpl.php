<?php
?>
<div id="block-<?php echo $block->module .'-'. $block->delta ?>" class="block block-<?php echo $block->module .' '. $block_zebra .' '. $block->region ?>">

  <?php if ($block->subject): ?>
    <h2 class="block-title"><?php echo $block->subject ?></h2>
  <?php endif; ?>

  <div class="block-content">
    <?php echo $block->content ?>
  </div>

  <?php echo $edit_links ?>

</div>
