<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
  "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
  <title><?php print $head_title; ?></title>
  <?php print $head; ?>
  <?php print $styles; ?>
  <!--[if IE]>
    <link rel="stylesheet" href="css/ie.css" type="text/css" media="screen, projection" />
  <![endif]-->
  <?php print $scripts; ?>

</head>

<body id="thingee" <?php print $page_classes; ?>>
  <div id="wrapper-header">
    <div class="container <?php print $body_classes; ?>">

      <!-- ** HEADER ** -->
      <div id="header" class="span-24 last">

        <!-- ** SECONDARY LINKS ** -->
        <?php if (!empty($secondary_links)): ?>
        <div id="secondary" class="span-24 last textright">
          <?php echo theme('links', $secondary_links, array('class' => 'links secondary-links')); ?>
        </div>
        <?php endif; ?>
        <!-- /secondary-links-->

        <!-- ** LOGO - SITE-NAME - SITE-SLOGAN ** -->
        <div id="logo" class="span-7 col">
          <a class="logo floatright" href="<?php print $base_path ?>" title="<?php print t('Home') ?>"><img src="<?php print $logo ?>" alt="<?php print t('Home') ?>" /></a>
        </div>
        <!-- /logo -->
        <div id="site-name" class="span-16 last">
          <h1 class="site-name floatleft"><a href="<?php print $base_path ?>" title="<?php print t('Home') ?>"><?php print $site_name ?></a></h1>
          <span class="slogan"><?php print $site_slogan ?></span>
        </div>
        <!-- /site-name - site-slogan -->

        <!-- ** PRIMARY LINKS ** -->
        <div id="primary" class="span-24 last">
          <?php if (!empty($primary_links)): ?>
          <?php echo theme('links', $primary_links, array('class' => 'links primary-links')); ?>
          <?php endif; ?>
        </div>
        <!-- /primary links -->

      </div><!-- /header -->
    </div><!-- /.container -->
  </div><!-- /wrapper-header -->

  <!-- ** MAIN CONTENT ** -->
  <div id="wrapper-content">
    <div class="container">

      <!-- ** CONTNET ** -->
      <div id="content" class="span-15">

        <?php print $breadcrumb ?>

        <?php if ($top_content) { ?>
        <div class="top-content"><?php print $top_content; ?>asdf asdf</div><hr/>
        <?php } ?>

        <?php if (!empty($messages)): print $messages; endif; ?>
			  <?php if (!empty($help)): print $help; endif; ?>

        <?php if ($title) {  ?>
        <h1 class="title"><?php print $title ?></h1>
        <?php } ?>

        <?php if ($tabs) { ?>
        <div class="tabs"><?php print $tabs ?></div>
        <?php } ?>

        <?php print $content; ?>

      </div><!-- /content -->

      <!-- SIDEBAR -->
      <div id="sidebar" class="span-8 last">
        <?php print $right; ?>
      </div>
      <!-- /sidebar -->

    </div><!-- /.container -->
  </div><!-- /wrapper-content -->

  <div id="wrapper-footer">
    <div class="container">
      <div id="footer" class="span-24 last">

        <?php if ($pre_footer) { ?>
        <div class="pre-footer"><?php print $pre_footer; ?></div><hr/>
        <?php } ?>

        <?php if ($mid_footer) { ?>
        <div class="mid-footer"><?php print $mid_footer; ?></div><hr/>
        <?php } ?>

        <?php if ($links_footer) { ?>
        <div class="links-footer"><?php print $links_footer; ?></div>
        <?php } ?>

        <div class="textright">
          <span class="quiet">Copyright &copy; 2009</span> |
          <a href="http://www.webthingee.com" class="quiet">webthingee.com</a> |
          <a href="http://www.blogthingee.com" class="quiet">blogthingee.com</a>
        </div>

      </div><!-- /footer -->
    </div><!-- /.container -->
  </div><!-- /wrapper-footer -->

<?php print $closure ?>
</body>
</html>